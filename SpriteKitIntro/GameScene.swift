//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by Harman Kaur on 2019-09-30.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    //has all ur game logic
    
    //MARK: SPrite variables
    //----------------------------
    var pikachu:SKSpriteNode!
    var square: SKSpriteNode!
    var highScoreLabel: SKLabelNode!
    
    //Constructor- setup your scene + sprites
    override func didMove(to view: SKView) {
        print("Hello World");
        //size is a global variable that gives you size of the screen
        //size.width = this.screenwidth in android
        //size.height = this.screenheight in android
        print("Screen Size (w , h:  \(size.width), \(size.height))")
        
        //ADD some sprites
        //-------------------------
        
        //make some text
        //----------------
        //1. make a label node
        self.highScoreLabel = SKLabelNode(text: "Score: 25")
        //2. configure the node
        //setting font size, color, position etc
        self.highScoreLabel.position = CGPoint(x: 100, y: 100)
        self.highScoreLabel.fontSize = 45
        self.highScoreLabel.fontColor = UIColor.yellow
        self.highScoreLabel.fontName = "Avenir"
        
        //3. show node on screen
        addChild(self.highScoreLabel)
        
        //draw a square
        //----------------------------
        //1. create the square sprite
        //Note: CGSize objects = rect object in java
        self.square = SKSpriteNode(color: UIColor.yellow, size: CGSize(width: 100, height: 100))
        //2. configure the square
        self.square.position = CGPoint(x: 300, y: 500)
        //3. Add the square to screen
        addChild(self.square)
        
        //draw a picture
        //-----------------------------
        
        //1. Make an image node
        self.pikachu = SKSpriteNode(imageNamed: "pikachu64")
        //2. configure the image node
        self.pikachu.position = CGPoint(x: 200, y: 323)
        //3. add image to the screen
        addChild(self.pikachu)
        
        //2. automatic movement( using built in spritekit functions)
        let upMoveAction = SKAction.moveBy(x: 0, y: 50, duration: 1)
        let rightMoveAction = SKAction.moveBy(x: 50, y: 0, duration: 1)
        let leftMoveAction = SKAction.moveBy(x: -100, y: 0, duration: 1)
        let animation = SKAction.sequence([upMoveAction, rightMoveAction, leftMoveAction, upMoveAction])
        self.square.run(animation)
         self.highScoreLabel.run(animation)
    }
    
    //MARK: update positions/redraw sprites function
    //-----------------------------------
    //Built in function that runs once per frame
    //similar to updatePositions() in android
    
    var pikachuDirection: String = "left"
    override func update(_ currentTime: TimeInterval) {
        //print("\(currentTime): NONSENSE")
        
        //types of movements
        //1. manual movement (similar to android)
        if(self.pikachuDirection == "right") {
             self.pikachu.position.x = self.pikachu.position.x + 1
            //check if pikachu touches the wall
            if(self.pikachu.position.x >= size.width)
            {
                self.pikachuDirection = "left"
            }
        }
        else
            if(self.pikachuDirection == "left") {
                self.pikachu.position.x = self.pikachu.position.x - 1
                if(self.pikachu.position.x <= 0)
                {
                    self.pikachuDirection = "right"
                }
        }
       
       
    }
    
    //MARK: detect user input
    //-------------------------------
    //touchesBegan = Event.ACTION_DOWN
    //used to detect when finger touches the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    //touchesEnded = Event.ACTION_UP
    //used to detect when finger lifts from screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Person touched the screen")
        
        //detect (x, y) position of where finger is
        //get the (x, y) position of the mouse
        //Spritekit will return the position as  a UITouch object
        let locationTouched = touches.first
        if (locationTouched == nil) {
            //some kind of error occurred when detecting the touch location
            return
        }
        
        //inside the UITouch object, look at the location property
        //the location property specifies what thing you are touching
        let mousePosition = locationTouched!.location(in: self)
        print("MOUSE X?  \(mousePosition.x)")
        print("MOUSE Y?  \(mousePosition.y)")
        print("------")

    }
}
